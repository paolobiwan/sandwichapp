//
//  FavoritesViewController.swift
//  SandwichApp
//
//  Created by Ivan Tilev on 24/02/2020.
//  Copyright © 2020 Ivan Tilev. All rights reserved.
//

import UIKit
import CoreData

// MARK: - Favorites View Controller
class FavoritesViewController: UIViewController {
    
    // MARK: - CoreData: Logic to add sanwiches to the database
    @IBOutlet weak var fieldName: UITextField!
    @IBOutlet weak var fieldIngredients: UITextField!
    @IBOutlet weak var fieldRecipe: UITextField!
    @IBOutlet weak var fieldCategory: UITextField!
    @IBOutlet weak var fieldImg: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // Saving Sandwich to CoreData
    @IBAction func saveSandwich_clicked(_ sender: UIButton) {
        
        guard !self.fieldName.text!.isEmpty else {return}
        guard !self.fieldIngredients.text!.isEmpty else {return}
        guard !self.fieldRecipe.text!.isEmpty else {return}
        guard !self.fieldCategory.text!.isEmpty else {return}
        guard !self.fieldImg.text!.isEmpty else {return}
        
        let name = self.fieldName.text!
        let ingredients = self.fieldIngredients.text!
        let recipe = self.fieldRecipe.text!
        let category = self.fieldCategory.text!
        let img = self.fieldImg.text!
        
        CoreDataController.shared.addSandwich(name: name, ingredients: ingredients, recipe: recipe, category: category, img: img)
        
    }
    
}
