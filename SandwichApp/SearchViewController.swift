//
//  SearchViewController.swift
//  SandwichApp
//
//  Created by Ivan Tilev on 24/02/2020.
//  Copyright © 2020 Ivan Tilev. All rights reserved.
//

import UIKit

// MARK: - Search View Controller
class SearchViewController: UIViewController, UITableViewDelegate {
    
    // Outlet
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchIngredients: UISearchBar!
    @IBOutlet weak var currentCategory: UITextField!
//    the array with all the sandwiches got from the database
    var arraySandwich = CoreDataController.shared.loadAllSandwichs2()
    
    let secondVC = FavoritesViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchIngredients.delegate = self
        backgroundImageView.alpha = 0.1
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "ReusableCell")
    }

// get sandwiches by ingredients and/or category
    func getSandwiches()-> Array<Sandwich>{
        var newArray = [Sandwich]()
        guard !self.searchIngredients.text!.isEmpty else {return newArray}
        
        let ingredientsToSearch = self.searchIngredients.text!
        print(ingredientsToSearch)
// if category is not used, use search function for all ingredients
        if self.currentCategory.text!.isEmpty {
            newArray = CoreDataController.shared.loadFromIngredients2(ingredient: ingredientsToSearch)
// else use the search function in the choosed category of ingredients
        }else{
            newArray = CoreDataController.shared.loadFromIngredientsAndCategory2(ingredient: ingredientsToSearch, category: currentCategory.text!)
        }
        return newArray
    }

}

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Code here
        return arraySandwich.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCell", for: indexPath) as! CustomCell
        let currentLastItem = arraySandwich[indexPath.row]
        
        cell.sandwichNameLabel.text = currentLastItem.name
        cell.categoryLabel.text = currentLastItem.category
        cell.imageCell.image = UIImage(named: currentLastItem.img!)
//        cell.imageCell.contentMode = .scaleToFill
        return cell
    }
    
    
}

extension SearchViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        arraySandwich = getSandwiches()
        tableView.reloadData() 
    }
    
    
}
